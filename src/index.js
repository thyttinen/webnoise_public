import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import './css/flexboxgrid.css';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import storeFactory from './store/index'
import { Provider } from 'react-redux'
import NoiseGL from './gl/NoiseGL'

import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const store = storeFactory()
NoiseGL.setUpStore(store)

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();

export {store}