import React from 'react'
import { findDOMNode } from 'react-dom'
import PropTypes from 'prop-types';
import {mix} from "../../logic/math";

class ImageCanvas extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor () {
    super()
    this.reDrawCanvas = this.reDrawCanvas.bind(this)
    this.passHeightMap = this.passHeightMap.bind(this)
  }

  componentDidUpdate(prevProps) {
    if(this.props.imageLayer.fileHandle !== prevProps.imageLayer.fileHandle){
      this.reDrawCanvas();
    }
  }

  componentDidMount() {
    this.reDrawCanvas();
  }

  reDrawCanvas() {

    const ref = findDOMNode(this)
    const ctx=ref.getContext("2d")

    if(!this.props.imageLayer || !this.props.imageLayer.fileHandle) {
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
      return;
    }

    ctx.fillStyle="rgba(127, 125, 139, 1.0)";
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    const reader  = new FileReader();
    const file = this.props.imageLayer.fileHandle;

    const img = new Image();

    img.onload = () => {
      ctx.drawImage(img,
        0, 0, img.width, img.height,
        0, 0, ctx.canvas.width, ctx.canvas.height)


      const imageData = ctx.getImageData(0,0,ctx.canvas.width,ctx.canvas.height)
      const heightMap = this.calculateHeightMap(imageData, ctx.canvas.width)
      this.passHeightMap(heightMap)
    }

    reader.onloadend = () => {
      img.src = reader.result;
    }

    reader.readAsDataURL(file);
  }

  calculateHeightMap(imageData, imageSize) {
    let heightMap = [];
    let numValues = imageSize * imageSize;
    for(let i = 0; i < numValues; i++) {
      let redIndex = i * 4;
      let heightValue = imageData.data[redIndex] / 255;
      let alphaValue = imageData.data[redIndex+3] / 255;
      heightMap[i] = mix(heightValue, 0.5, 1.0 - alphaValue);
    }
    return heightMap
  }

  passHeightMap(heightMap) {
    this.props.onHeightMapLoad(heightMap);
  }

  render() {
    return (
      <canvas width={256} height={256}></canvas>
    );
  }

}


ImageCanvas.propTypes = {
  imageLayer: PropTypes.object,
  onHeightMapLoad : PropTypes.func
};

export default ImageCanvas;
