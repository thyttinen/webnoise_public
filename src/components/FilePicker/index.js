import React from 'react'
import { RaisedButton } from 'material-ui'

class FilePicker extends React.Component { // eslint-disable-line react/prefer-stateless-function

  handleChooseFileClick() {
    setTimeout(() => {
      this._inputLabel.click();
    }, 200);
  }

  handleFileChange(e) {
    var fileHandle = e.target.files[0];
    console.log(fileHandle);
    this.props.onFileLoaded(fileHandle);
  }

  render() {
    return (
      <div className="bottom-buttons">
        <input
          type="file" name={this.props.inputId ? this.props.inputId : "default"} id={this.props.inputId ? this.props.inputId : "default"}
          className="input-file"
          style={{display : "none"}}
          onChange={this.handleFileChange.bind(this)}
        />
        <label htmlFor={this.props.inputId ? this.props.inputId : "default"} ref={x => this._inputLabel = x}>
          <RaisedButton label="Load" onTouchTap={this.handleChooseFileClick.bind(this)}/>
        </label>
      </div>
    );
  }

}


export default FilePicker;
