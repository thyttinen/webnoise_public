import React from 'react'
import { findDOMNode } from 'react-dom'
import { evaluateRd } from '../../logic/redistribution'
import {evaluateMaskFor, evaluateNoiseRd, evaluateSimplex} from '../../logic/noise'
import PropTypes from 'prop-types';
import {layerById} from "../../util";

class NoiseCanvas extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor () {
    super()
    this.reDrawCanvas = this.reDrawCanvas.bind(this)
  }

  componentDidUpdate() {
    this.reDrawCanvas();
  }

  componentDidMount() {
    this.reDrawCanvas();
  }

  reDrawCanvas() {
    const ref = findDOMNode(this)
    const ctx=ref.getContext("2d")


    for(let i = 0; i < 256; i++) {
      for(let j = 0; j < 256; j++) {
        ctx.fillStyle=this.getFillColorAt(i,j);
        ctx.fillRect(i,j,1,1);
      }
    }
   }

  getFillColorAt (i, j) {
    let {freq, points} = this.props.noiseLayer
    let val = evaluateNoiseRd(i, j, freq, points, this.props.layerIndex)

    if(this.props.noiseLayer.mask.maskingLayerId) {
      let maskRange = this.props.noiseLayer.mask.range;
      let maskLayerVal = evaluateMaskFor(i, j, this.props.noiseLayer)
      if(maskLayerVal < maskRange[0] || maskLayerVal > maskRange[1]) {
        val = 0.5;
      }
    }

    val = Math.floor(255 * val)
    return 'rgb('+ val + ',' + val +  ',' + val + ')';
  }

  render() {
    return (
      <canvas width={256} height={256}></canvas>
    );
  }

}


NoiseCanvas.propTypes = {
  layerIndex : PropTypes.number,
  noiseLayer: PropTypes.object
};

export default NoiseCanvas;
