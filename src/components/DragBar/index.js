/**
 *
 * DragBarControl
 *
 */

import React from 'react';
import DragBar from './DragBar'
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import { fromJS } from 'immutable';
import { bindThis } from '../../util'
import { clamp } from '../../logic/math'

const DragBarContainer = styled.div`
  display: flex;
  flex-direction: row;
  user-select: none;
`;

class DragBarControl extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      pointHeights: props.points.map(p => 0)
    };
    bindThis(this, 'onMouseDown')
    bindThis(this, 'onMouseUp')
    bindThis(this, 'onMouseMove')
  }

  componentDidMount() {
    this.setState({pointHeights : this.getPointHeights(this.props.points)});
  }

  onMouseDown(e) {
    if (e.button !== 0) return;
    document.addEventListener('mousemove', this.onMouseMove)
    document.addEventListener('mouseup', this.onMouseUp)
    e.preventDefault();
  }

  onMouseUp(e) {
    document.removeEventListener('mousemove', this.onMouseMove)
    document.removeEventListener('mouseup', this.onMouseUp)
    e.preventDefault();
  }

  componentWillReceiveProps({points}) {
    this.setState({pointHeights : this.getPointHeights(points)});
  }

  onMouseMove(e) {
    const ref = ReactDOM.findDOMNode(this)
    const refBox = ref.getBoundingClientRect()
    const childBox = ref.children[0].getBoundingClientRect()

    const mouseX = e.clientX;
    const mouseY = e.clientY;

    const containerX = refBox.left;
    const containerWidth = refBox.width;
    const childWidth = childBox.width;

    const containerY = refBox.top;
    const containerHeight = refBox.height;


    if(mouseX > containerX && mouseX < containerX+containerWidth) {
      const x = (mouseY - containerY) / containerHeight;
      const amount = 1 - clamp(x, 0, 1);

      let pointIndex = -1;

      for(let i = 0; i < this.props.points.length; i++) {
        if(mouseX < containerX + (childWidth * (i+1))) {
          pointIndex = i;
          break;
        }
      }
      if(pointIndex !== -1) {
        const newHeight = amount * containerHeight;
        const newPointHeights = this.state.pointHeights.slice()
        newPointHeights[pointIndex] = newHeight
        this.setState({pointHeights: newPointHeights})
        const newPoints = fromJS(this.props.points).setIn([pointIndex, 'y'], amount).toJS()
        this.props.onPointsChange(newPoints)
      }
    }

    e.preventDefault();
  }

  getPointHeights(points) {
    const ref = ReactDOM.findDOMNode(this)
    const containerHeight = ref.clientHeight
    const newPointHeights = points.map(function(point) {
      return containerHeight * clamp(point.y, 0, 1)
    });
    return newPointHeights;
  }


  render() {
    let parent = this;
    return (
      <DragBarContainer onMouseDown={this.onMouseDown}>
        {this.props.points.map(function(point, i){
          return <DragBar key={point.x} point={point} height={parent.state.pointHeights[i]} />;
        })}
      </DragBarContainer>
    );
  }

}

export default DragBarControl;
