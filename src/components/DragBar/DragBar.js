/**
 *
 * DragBar
 *
 */

import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const BarContainer = styled.div`
      height: 150px;
      flex-grow: 1;
      display: flex;
      align-items: flex-end;
    `;

const Bar = styled.div`
      border-top-right-radius: 2px;
      border-top-left-radius: 2px;
      border: 1px solid rgba(255, 255, 255, 0.7);
      background-color : #607D8B;
      flex-grow: 1;
      color: white;
      text-align: center;
      padding-top: 2px;
      box-sizing: border-box;
      font-size: 7px;
    `;

class DragBar extends React.Component { // eslint-disable-line react/prefer-stateless-function

  getHeightStyle() {
    return {
      height : this.props.height
    }
  }

  render() {
    const {x, y} = this.props.point;
    return (
      <BarContainer>
        <Bar style={this.getHeightStyle()}>{y.toFixed(2)}</Bar>
      </BarContainer>
    );
  }

}

DragBar.propTypes = {
  point: PropTypes.object
};

export default DragBar;
