import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NoiseGL from './gl/NoiseGL'
import {Paper } from 'material-ui'
import NoiseViewMenuTabs from './containers/NoiseView/index'
import SettingsMenuTabs from './containers/Settings/index'
import LayerSelector from './containers/LayerSelector'
import { getMuiTheme } from 'material-ui/styles/index'
import {store} from './index.js'

const muiTheme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: "#607D8B",
    primary2Color: "#455A64",
    primary3Color: "#CFD8DC",
    accent1Color: "#9E9E9E",
    textColor: "#212121",
    alternateTextColor: "#FFFFFF"
  },
});

class App extends Component {

  componentDidMount() {
    NoiseGL.init("glCanvas");
    NoiseGL.updateObjects(store.getState())
    NoiseGL.render();
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div className="container-fluid">
          <div className="row">
            <Paper zDepth={2} className="col-xs-2" style={{padding : 0}}>
              <LayerSelector/>
            </Paper>
            <div className="col-xs-10">
              <div className="row">
                <div className="col-xs-12">
                  <Paper zDepth={2}>
                      <NoiseViewMenuTabs/>
                  </Paper>
                </div>
              </div>
              <div className="row" style={{marginTop : '2rem'}}>
                <div className="col-xs-12">
                  <Paper zDepth={2}>
                    <SettingsMenuTabs/>
                  </Paper>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
