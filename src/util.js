const activeLayer = function(state) {
  return state.layers.find(l => l.id === state.selectedLayerId)
}

const activeLayerIndex = function(state) {
  return state.layers.indexOf(state.layers.find(l => l.id === state.selectedLayerId))
}

const layerIndex = function(state, layer) {
  return state.layers.indexOf(state.layers.find(l => l.id === layer.id))
}

const layerById = function(state, id) {
  return state.layers.find(l => l.id === id)
}

const bindThis = function(that, fn) {
  that[fn] = that[fn].bind(that);
}

export {activeLayer, activeLayerIndex, layerById, bindThis, layerIndex}