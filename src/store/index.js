import { createStore, combineReducers, applyMiddleware } from 'redux'
import {selectedLayerId, layers, combinedMode, renderingSettings} from './reducers'
import getProcessedNoise from '../gl/ProcessedNoise'
import constants from '../constants'

const createLayer = id => ({ id, noise : getProcessedNoise(), image : {fileHandle : null, windowFn : constants.WINDOW_HANN}});

const stateData = {selectedLayerId : 1, layers : [

  createLayer(1),
  createLayer(2)

  ], renderingSettings : { textureLimits : [0.0, 0.25, 0.5, 1.0], renderSize : 256, texturesEnabled : false, noiseZ : 0 }};

const logger = store => next => action => {
  let result
  console.groupCollapsed("dispatching", action.type)
  console.log('prev state', store.getState())
  console.log('action', action)
  result = next(action)
  console.log('next state', store.getState())
  console.groupEnd()
  return result
}

const saver = store => next => action => {
  let result = next(action)
  localStorage['redux-store'] = JSON.stringify(store.getState())
  return result
}

const storeFactory = (initialState=stateData) =>
  applyMiddleware(logger)(createStore)(
    combineReducers({selectedLayerId, layers, combinedMode, renderingSettings}),
    initialState
  )

export {createLayer}

export default storeFactory