import C from '../constants'

const pointsChanged = (points, layerId) =>
  ({
    type: C.SET_POINT_PROPS,
    points,
    layerId
  })

const freqChanged = (freq, layerId) =>
  ({
    type: C.SET_FREQ,
    freq,
    layerId
  })

const weightChanged = (weight, layerId) =>
  ({
    type: C.SET_WEIGHT,
    weight,
    layerId
  })

const selectLayer = (layerId) =>
  ({
    type: C.SET_ACTIVE_LAYER_ID,
    layerId
  })

const deleteLayer = (layerId) =>
  ({
    type: C.DELETE_LAYER,
    layerId
  })

const addLayer = () =>
  ({
    type: C.ADD_LAYER,
  })

const setCombinedMode = (active) =>
  ({
    type: C.SET_COMBINED_MODE,
    active
  })

const setFileHandle = (fileHandle, layerId) =>
  ({
    type: C.SET_FILE_HANDLE,
    fileHandle,
    layerId
  })

const setHeightMap = (heightMap, layerId) =>
  ({
    type: C.SET_HEIGHT_MAP,
    heightMap,
    layerId
  })

const setWindowFn = (windowFn, layerId) =>
  ({
    type: C.SET_WINDOW_FN,
    windowFn,
    layerId
  })

const setMaskRange = (range, layerId) =>
  ({
    type: C.SET_MASK_RANGE,
    range,
    layerId
  })

const setMaskingLayerId = (maskingLayerId, layerId) =>
  ({
    type: C.SET_MASKING_LAYER_ID,
    maskingLayerId,
    layerId
  })

const saveState = () => ({
  type : C.SAVE_STATE
})

const loadState = (state) => ({
  type : C.LOAD_STATE,
  state : state
})

const setTextureLimits = (textureLimits) =>
  ({
    type: C.SET_TEXTURE_LIMITS,
    textureLimits
  })

const setRenderSize = (renderSize) =>
  ({
    type: C.SET_RENDER_SIZE,
    renderSize
  })

const setTexturesEnabled = (texturesEnabled) =>
  ({
    type: C.SET_TEXTURES_ENABLED,
    texturesEnabled
  })

const setNoiseZ = (noiseZ) =>
  ({
    type: C.SET_NOISE_Z,
    noiseZ
  })


setMaskingLayerId

export {pointsChanged, freqChanged, weightChanged, selectLayer, deleteLayer, addLayer, setCombinedMode,
  setFileHandle, setHeightMap, setWindowFn, setMaskRange, setMaskingLayerId, saveState, loadState, setTextureLimits,
  setRenderSize, setTexturesEnabled, setNoiseZ}
