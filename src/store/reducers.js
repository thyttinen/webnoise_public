import C from '../constants'
import * as Immutable from '../../node_modules/immutable/dist/immutable'
import {fromJS} from '../../node_modules/immutable/dist/immutable'
import {createLayer} from './index'

let highestId = 3;

export const points = (state = [], action) => {
  switch (action.type) {
    case C.SET_POINT_PROPS :
      return [...action.points]
    default:
      return state
  }
}

export const layerNoise = (state = {}, action) => {
  let newState;
  switch (action.type) {
    case C.SET_POINT_PROPS :
      return Immutable.Map(state).set("points", points(state.points, action)).toJS() //todo all immutable
    case C.SET_FREQ :
      return Immutable.Map(state).set("freq", action.freq).toJS()
    case C.SET_WEIGHT :
      return Immutable.Map(state).set("weight", action.weight).toJS()
    case C.SET_MASK_RANGE :
      return fromJS(state).setIn(["mask", "range"], action.range).toJS();
    case C.SET_MASKING_LAYER_ID :
      return fromJS(state).setIn(["mask", "maskingLayerId"], action.maskingLayerId).toJS();
    default:
      return state
  }
}

export const layerImage = (state = {}, action) => {
  switch (action.type) {
    case C.SET_FILE_HANDLE :
      return Immutable.Map(state).set("fileHandle", action.fileHandle).toJS()
    case C.SET_HEIGHT_MAP :
      console.log("setting height map", action.heightMap)
      return Immutable.Map(state).set("heightMap", action.heightMap).toJS()
    case C.SET_WINDOW_FN :
      return Immutable.Map(state).set("windowFn", action.windowFn).toJS()
    default:
      return state
  }
}

export const layer = (state = {}, action) => {
  if(action.layerId !== state.id) {
    return state;
  }
  return Immutable.Map(state)
    .set("noise", layerNoise(state.noise, action))
    .set("image", layerImage(state.image, action)).toJS();
}

export const layers = (state = [], action) => {
  if(action.type === C.LOAD_STATE) {
    highestId+=20 //todo
    return action.state.layers;
  }
  if(action.type === C.DELETE_LAYER && state.length > 1) {
    return state.filter(l => l.id !== action.layerId)
  }
  if(action.type === C.ADD_LAYER && state.length < 10) {
    return Immutable.List(state).push(createLayer(++highestId)).toJS();
  }
  return state.map(l => layer(l, action));
}

export const selectedLayerId = (state = 1, action) => {
  if(action.type === C.LOAD_STATE) {
    return action.state.selectedLayerId;
  }
  switch (action.type) {
    case C.SET_ACTIVE_LAYER_ID :
      return action.layerId;
    default :
      return state
  }
}

export const combinedMode = (state = false, action) => {
  if(action.type === C.LOAD_STATE) {
    return action.state.combinedMode;
  }
  switch (action.type) {
    case C.SET_COMBINED_MODE :
      return action.active;
    default :
      return state
  }
}

export const renderingSettings = (state = {}, action) => {
  if(action.type === C.LOAD_STATE) {
    return action.state.renderingSettings ?  action.state.renderingSettings : state;
  }
  switch (action.type) {
    case C.SET_TEXTURE_LIMITS :
      return Immutable.Map(state).set("textureLimits", action.textureLimits).toJS();
    case C.SET_RENDER_SIZE :
      return Immutable.Map(state).set("renderSize", action.renderSize).toJS();
    case C.SET_TEXTURES_ENABLED :
      return Immutable.Map(state).set("texturesEnabled", action.texturesEnabled).toJS();
    case C.SET_NOISE_Z :
      return Immutable.Map(state).set("noiseZ", action.noiseZ).toJS();
    default :
      return state
  }
}