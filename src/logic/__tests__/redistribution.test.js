import {getEqualizedRedistributionPoints} from "../redistribution";
import {findMaskRange} from "../solver";

it('get defined redistribution points', () => {
  const values = [0,0,0,1,1,1]
  const numPoints = 3;
  expect(getEqualizedRedistributionPoints(values, numPoints)).toBeTruthy()
  expect(getEqualizedRedistributionPoints(values, numPoints).length).toBe(numPoints)
});


it('test mask range calculation', () => {
  const values = [0.509, 0.504, 1, 1, 1, 0.499];
  const maskValues = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6];
  expect(findMaskRange(values, maskValues)).toEqual([0.3, 0.5]);
});

it('test mask range for empty mask', () => {
  const values = [0.509, 0.504, 1, 1, 1, 0.499];
  const maskValues = null;
  expect(findMaskRange(values, maskValues)).toEqual([0.0, 1.0]);
});