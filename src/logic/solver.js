import * as fft from 'jsfft'
import {flatTop, getAveragedFrequencySpectrumInfo, getFrequencySpectrumInfo, hamming, hann, mean} from './math'
import constants from '../constants'

const solveStats = (heightMap, windowFn) => {
  const numPoints = 256;

  let window = () => 1;
  if(windowFn === constants.WINDOW_HANN) {
    window = (i, numPoints) => hann(i, numPoints);
  } else if(windowFn === constants.WINDOW_HAMMING) {
    window = (i, numPoints) => hamming(i, numPoints);
  } else if(windowFn === constants.WINDOW_FLAT_TOP) {
    window = (i, numPoints) => flatTop(i, numPoints);
  }

  const dataRows = [];
  for(let i = 0; i < 256; i++) {
    const row = heightMap.slice(i * numPoints, i * numPoints + 256);
    const meanValue = mean(row)

    const data = new fft.ComplexArray(numPoints).map((value, i) => {
      let val = row[i] - meanValue;
      value.real = window(i, numPoints) * val;
    });

    dataRows.push(data);
  }

  return getFrequencySpectrumInfo(dataRows[0]);
}

const difference = function (a, b) { return Math.abs(a - b); }

const findMaskRange = (heightMap, maskingMap) => {

  const MIN_DIFF_FROM_CENTER = 0.01;
  const NO_MASK_MIN_FINAL_DIFF = 0.1;

  if(!maskingMap) {
    return [0,1];
  } else {
    let maskMin = 1.0;
    let maskMax = 0.0;
    for(let i = 0; i < heightMap.length; i++) {
      let heightMapValue = heightMap[i];
      let maskMapValue = maskingMap[i];
      if(difference(heightMapValue, 0.5) > MIN_DIFF_FROM_CENTER) {
        if(maskMapValue > maskMax) {
          maskMax = maskMapValue;
        }
        if(maskMapValue < maskMin) {
          maskMin = maskMapValue;
        }
      }
    }
    if(difference(maskMin, 0) < NO_MASK_MIN_FINAL_DIFF) {
      maskMin = 0.0;
    }
    if(difference(maskMax, 1) < NO_MASK_MIN_FINAL_DIFF) {
      maskMax = 1.0;
    }
    return [maskMin, maskMax];
  }
}


export {solveStats, findMaskRange}