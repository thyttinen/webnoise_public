import SimplexNoise from 'simplex-noise'
import {evaluateRd} from "./redistribution";
import {layerById, layerIndex} from "../util";
import {store} from '../index'

Math.seedrandom('webnoise');
let simplex = new SimplexNoise()

//0 .. 1
const evaluateSimplex = function(x, y, freq, offsetIndex, globalZOffset = 0) {
  return (simplex.noise3D((x + offsetIndex*1024) * freq, y * freq, globalZOffset) + 1) / 2;
}

const evaluateNoiseRd = function(i, j, freq, points, layerIndex) {
  let noise = evaluateSimplex(i, j, freq, layerIndex)
  return evaluateRd(noise, points)
}

const evaluateMaskFor = function(i, j, noise) {
  let maskId = noise.mask.maskingLayerId
  let maskingLayer = layerById(store.getState(), maskId)
  let {freq, points} = maskingLayer.noise
  let index = layerIndex(store.getState(), maskingLayer)
  return evaluateNoiseRd(i, j, freq, points, index)
}

export {evaluateSimplex, evaluateNoiseRd, evaluateMaskFor}