import * as fft from 'jsfft'
const lerp = function (value1, value2, amount) {
  amount = amount < 0 ? 0 : amount;
  amount = amount > 1 ? 1 : amount;
  return value1 + (value2 - value1) * amount;
}

const cosInterpolate = (y1, y2, mu) => {
  const mu2 = (1 - Math.cos(mu * Math.PI)) / 2;
  return (y1 * (1 - mu2)) + (y2 * mu2);
};

const cubicInterpolate = function (y0, y1, y2, y3, mu)
{
  let a0,a1,a2,a3,mu2;

  mu2 = mu*mu;
  a0 = y3 - y2 - y0 + y1;
  a1 = y0 - y1 - a0;
  a2 = y2 - y0;
  a3 = y1;

  return(a0*mu*mu2+a1*mu2+a2*mu+a3);
}

const clamp = function(x, minVal, maxVal) {
  return Math.min(Math.max(x, minVal), maxVal);
}

const mean = (values) => values.reduce((acc, v) => acc + v, 0) / values.length

const getFrequencySpectrumInfo = function(data) {
  const frequencies = data.FFT();
  const halfLen = frequencies.real.length/2;

  let freqs = [];

  for(let i = 0; i < halfLen; i++) {
    let amp = frequencies.real[i]
    let absAmp = Math.pow(Math.abs(amp), 2);
    freqs.push(absAmp);
  }

  //chartData[0].amp = 0; //clean DC

  return {chartData : getChartData(freqs), stats : getStats(freqs)};
}

const getAveragedFrequencySpectrumInfo = function(dataRows) {

  let freqTotals = new Array(dataRows[0].real.length/2).fill(0);

  dataRows.forEach(function(data) {
    const frequencies = data.FFT();
    const halfLen = frequencies.real.length/2;

    for(let i = 0; i < halfLen; i++) {
      let absAmp = Math.pow(Math.abs(frequencies.real[i]), 2);
      freqTotals[i] += absAmp;
    }
  });

  let freqAverages = freqTotals.map(t => t / freqTotals.length)

  //chartData[0].amp = 0; //clean DC

  return {chartData : getChartData(freqAverages), stats : getStats(freqAverages)};
}

const getChartData = function (freqs) {
  return freqs.map((a, i) => ({amp : a, freq : relativeFreq(i, freqs.length)}));
}

const getStats = function(freqs) {
  const stats = {topAmplitudeFreq : 0, highestNonZeroFreq : 0}
  let topAmplitude = 0;
  for(let i = 0; i < freqs.length; i++) {
    const absAmp = freqs[i];
    if(i > 0 && absAmp > topAmplitude) {
      topAmplitude = absAmp
      stats.topAmplitudeFreq = relativeFreq(i, freqs.length);
    }
    if(absAmp > 0.1) {
      stats.highestNonZeroFreq = relativeFreq(i, freqs.length);
    }
  }
  return stats;
}

const relativeFreq = (freqIndex, numFreqs) => freqIndex / (numFreqs-1)

const initComplexArray = (numPoints, fn) => {
  return new fft.ComplexArray(numPoints).map((value, i) => {
    value.real = fn(i);
  });
}

const hann = function(n, N) {
 return 0.5 * (1 - Math.cos( (2*Math.PI * (n/N))));
}

const flatTop = function(n, N) {
  const a0 = 1;
  const a1 = 1.93;
  const a2 = 1.29;
  const a3 = 0.388;
  const a4 = 0.028;

  const cos = Math.cos;
  const PI = Math.PI;
  const N_1 = N -1;

  return a0 - a1 * cos( 2*PI*n / N_1 ) + a2 * cos( 4*PI*n / N_1 ) - a3 * cos( 6*PI*n / N_1 ) + a4 * cos( 8*PI*n / N_1 );
}

const hamming = function(n, N) {
  const A = 0.53836;
  const B = 0.46164;

  const cos = Math.cos;
  const PI = Math.PI;
  const N_1 = N -1;

  return A - B * cos(2*PI*n / N_1);
}

function linearToLog(value, linMin, linMax, logMin, logMax) {
  let minp = linMin;
  let maxp = linMax;

  let minv = Math.log(logMin);
  let maxv = Math.log(logMax);

  let scale = (maxv-minv) / (maxp-minp);

  return Math.exp(minv + scale*(value-minp));
}

function logToLinear(value, linMin, linMax, logMin, logMax) {
  let minp = linMin;
  let maxp = linMax;

  let minv = Math.log(logMin);
  let maxv = Math.log(logMax);

  let scale = (maxv-minv) / (maxp-minp);

  return (Math.log(value)-minv) / scale + minp;
}

function mix(component1, component2, amount) {
  let oneMinusAmount = 1.0 - amount;
  return component1 * oneMinusAmount + component2 * amount;
}

function smoothStep(x, edge0, edge1) {
  let t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
  return t * t * (3.0 - 2.0 * t);
}

function smoothStep2(x, edge0, edge1, edge2, edge3) {
  return smoothStep(x, edge0, edge1) * (1 - smoothStep(x, edge2, edge3));
}


export {lerp, cosInterpolate, cubicInterpolate, clamp, getFrequencySpectrumInfo, initComplexArray, hann, mean, linearToLog, logToLinear, getAveragedFrequencySpectrumInfo, flatTop, hamming, mix, smoothStep, smoothStep2}