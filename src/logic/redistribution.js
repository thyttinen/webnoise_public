import {clamp, cubicInterpolate, lerp} from './math'

const getEqualizedRedistributionPoints = function (inputValues, numPoints) {
  let points = [];
  let numValues = inputValues.length;
  let sampleOffset = numValues / numPoints;
  let sortedValues = [...inputValues].sort();

  for(let i = 0; i < numPoints; i++) {
    let value = sortedValues[Math.floor((i*sampleOffset) + (sampleOffset / 2))];
    points.push({x : value, y : value});
  }

  return points;
}

const evaluateRd = function (value, points) {
  let endPoint = findEnd(value, points);
  let endPointIndex = points.indexOf(endPoint);
  let startPointIndex = endPointIndex-1;
  if(startPointIndex < 0) {
    startPointIndex = 0;
  }
  let startPoint = points[startPointIndex];

  let startX = startPoint.x;
  let endX = endPoint.x;

  let y1 = startPoint.y;
  let y2 = endPoint.y;

  let alpha = clamp((value - startX) / (endX - startX), 0, 1);

  return lerp(y1, y2, alpha);
}

const findEnd = function(value, points) {
  return binarySearchByValue(value, points);
}

const binarySearchByValue = function(value, points) {
  let low = 0, high = points.length;
  while (low !== high) {
    let mid = Math.floor((low + high) / 2);
    if (points[mid].x < value) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }
  let maxIndex = points.length-1;
  return points[low > maxIndex ? maxIndex : low];
}

export {getEqualizedRedistributionPoints, evaluateRd}