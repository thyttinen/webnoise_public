import * as THREE from 'three'
import OrbitControls from 'three-orbitcontrols'
import { activeLayer } from '../util'
import { evaluateRd } from '../logic/redistribution'
import { evaluateSimplex } from '../logic/noise'
import MaterialModifier from 'three-material-modifier';
import {mix, smoothStep, smoothStep2} from "../logic/math"

const noiseFunctions = `


//noise start

//
// Description : Array and textureless GLSL 2D/3D/4D simplex
//               noise functions.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

vec4 mod289(highp vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0; }

float mod289(highp float x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0; }

vec4 permute(highp vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

float permute(highp float x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(highp vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

float taylorInvSqrt(highp float r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec4 grad4(highp float j, highp vec4 ip)
  {
  const vec4 ones = vec4(1.0, 1.0, 1.0, -1.0);
  vec4 p,s;

  p.xyz = floor( fract (vec3(j) * ip.xyz) * 7.0) * ip.z - 1.0;
  p.w = 1.5 - dot(abs(p.xyz), ones.xyz);
  s = vec4(lessThan(p, vec4(0.0)));
  p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www;

  return p;
  }

// (sqrt(5) - 1)/4 = F4, used once below
#define F4 0.309016994374947451

float snoise(highp vec4 v)
  {
  const vec4  C = vec4( 0.138196601125011,  // (5 - sqrt(5))/20  G4
                        0.276393202250021,  // 2 * G4
                        0.414589803375032,  // 3 * G4
                       -0.447213595499958); // -1 + 4 * G4

// First corner
  vec4 i  = floor(v + dot(v, vec4(F4)) );
  vec4 x0 = v -   i + dot(i, C.xxxx);

// Other corners

// Rank sorting originally contributed by Bill Licea-Kane, AMD (formerly ATI)
  vec4 i0;
  vec3 isX = step( x0.yzw, x0.xxx );
  vec3 isYZ = step( x0.zww, x0.yyz );
//  i0.x = dot( isX, vec3( 1.0 ) );
  i0.x = isX.x + isX.y + isX.z;
  i0.yzw = 1.0 - isX;
//  i0.y += dot( isYZ.xy, vec2( 1.0 ) );
  i0.y += isYZ.x + isYZ.y;
  i0.zw += 1.0 - isYZ.xy;
  i0.z += isYZ.z;
  i0.w += 1.0 - isYZ.z;

  // i0 now contains the unique values 0,1,2,3 in each channel
  vec4 i3 = clamp( i0, 0.0, 1.0 );
  vec4 i2 = clamp( i0-1.0, 0.0, 1.0 );
  vec4 i1 = clamp( i0-2.0, 0.0, 1.0 );

  //  x0 = x0 - 0.0 + 0.0 * C.xxxx
  //  x1 = x0 - i1  + 1.0 * C.xxxx
  //  x2 = x0 - i2  + 2.0 * C.xxxx
  //  x3 = x0 - i3  + 3.0 * C.xxxx
  //  x4 = x0 - 1.0 + 4.0 * C.xxxx
  vec4 x1 = x0 - i1 + C.xxxx;
  vec4 x2 = x0 - i2 + C.yyyy;
  vec4 x3 = x0 - i3 + C.zzzz;
  vec4 x4 = x0 + C.wwww;

// Permutations
  i = mod289(i);
  float j0 = permute( permute( permute( permute(i.w) + i.z) + i.y) + i.x);
  vec4 j1 = permute( permute( permute( permute (
             i.w + vec4(i1.w, i2.w, i3.w, 1.0 ))
           + i.z + vec4(i1.z, i2.z, i3.z, 1.0 ))
           + i.y + vec4(i1.y, i2.y, i3.y, 1.0 ))
           + i.x + vec4(i1.x, i2.x, i3.x, 1.0 ));

// Gradients: 7x7x6 points over a cube, mapped onto a 4-cross polytope
// 7*7*6 = 294, which is close to the ring size 17*17 = 289.
  vec4 ip = vec4(1.0/294.0, 1.0/49.0, 1.0/7.0, 0.0) ;

  vec4 p0 = grad4(j0,   ip);
  vec4 p1 = grad4(j1.x, ip);
  vec4 p2 = grad4(j1.y, ip);
  vec4 p3 = grad4(j1.z, ip);
  vec4 p4 = grad4(j1.w, ip);

// Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;
  p4 *= taylorInvSqrt(dot(p4,p4));

// Mix contributions from the five corners
  vec3 m0 = max(0.6 - vec3(dot(x0,x0), dot(x1,x1), dot(x2,x2)), 0.0);
  vec2 m1 = max(0.6 - vec2(dot(x3,x3), dot(x4,x4)            ), 0.0);
  m0 = m0 * m0;
  m1 = m1 * m1;
  return 49.0 * ( dot(m0*m0, vec3( dot( p0, x0 ), dot( p1, x1 ), dot( p2, x2 )))
               + dot(m1*m1, vec2( dot( p3, x3 ), dot( p4, x4 ) ) ) ) ;

  }

//noise end
      `;

let scene, camera, renderer;
let controls;
let chunk1;

let material, wireframeMaterial;

let texture0, texture1, texture2;

let previousSize = 256;

let globalNoiseZOffset = 0;

class Chunk {

  constructor (material, position, size) {
    let geometry = new THREE.PlaneGeometry( size, size, size/2, size/2 );
    let mesh = new THREE.Mesh( geometry, material );
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    mesh.position.set(position.x, position.y, position.z)
    this.position = position;
    this.mesh = mesh;
    this.geometry = geometry;
  }

}

const initScene = function(domElementId) {
  let domElement = document.getElementById(domElementId);

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera( 75, domElement.offsetWidth / domElement.offsetHeight, 1, 10000 );
  camera.position.z = 200;
  camera.lookAt(new THREE.Vector3(0,50,0))

  initObjects();

  renderer = new THREE.WebGLRenderer();
  renderer.setSize( domElement.offsetWidth, domElement.offsetHeight );

  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  renderer.setClearColor( 0xffffff, 1);

  domElement.appendChild( renderer.domElement );

  controls = new OrbitControls(camera, renderer.domElement);
  controls.target = new THREE.Vector3(0, 0, 0);

  window.addEventListener( 'resize', onWindowResize(domElement), false );
}

const init = function (domElementId) {
  let loader = new THREE.TextureLoader()
  texture0 = loader.load('textures/texture0.jpg')
  texture0.wrapS = texture0.wrapT = THREE.RepeatWrapping;
  texture0.offset.set( 0, 0 );
  texture0.repeat.set( 1, 1 );

  texture1 = loader.load('textures/texture1.jpg')
  texture1.wrapS = texture1.wrapT = THREE.RepeatWrapping;
  texture1.offset.set( 0, 0 );
  texture1.repeat.set( 1, 1 );

  texture2 = loader.load('textures/texture2.jpg')
  texture2.wrapS = texture2.wrapT = THREE.RepeatWrapping;
  texture2.offset.set( 0, 0 );
  texture2.repeat.set( 1, 1 );


  initScene(domElementId)
}


const onWindowResize = domElement => () => {
  const w = domElement.offsetWidth;
  const h = domElement.offsetHeight;
  camera.left = w / - 2;
  camera.right = w / 2;
  camera.top = h / 2;
  camera.bottom = h / - 2;
  camera.aspect = w/h;
  camera.updateProjectionMatrix();
  renderer.setSize( w, h );
}

const initObjects = function() {

  MaterialModifier.defineFragmentHooks({
    saveColor: 'insertbefore:#include <map_fragment>',
    setTexture: 'insertafter:#include <map_fragment>'
  });

  let CustomLambertMaterial = MaterialModifier.extend( THREE.MeshLambertMaterial, {
    uniforms: {
      texture0: { type: "t", value: texture0},
      texture1: { type: "t", value: texture1},
      texture2: { type: "t", value: texture2}, //todo
      textureLimits: { type: "v3", value : new THREE.Vector3(40,100,0)}
    },

    vertexShader: {
      uniforms : `
        varying highp vec3 v_position;
        varying highp vec3 v_normal;
        uniform sampler2D texture0;
        uniform sampler2D texture1;
        uniform sampler2D texture2;
      `,
      preTransform  : `
        v_position = position;
        v_normal = normal;
      `,
    },
    fragmentShader: {
      functions: `
      vec4 samplePlanar(sampler2D sampler, highp vec3 coords, highp vec3 blending, highp float zoom)
          {
            vec4 xaxis = texture2D(sampler, coords.yz * zoom);
            vec4 yaxis = texture2D(sampler, coords.xz * zoom);
            vec4 zaxis = texture2D(sampler, coords.xy * zoom);
            vec4 normalDiffuse = xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;
            return normalDiffuse;
          }
          
      vec4 sampleRegular(sampler2D sampler, highp vec2 vUv, highp vec3 blending, highp float zoom)
          {
            return texture2D(sampler, vUv);
          }
      ` + noiseFunctions,
      uniforms : `
        uniform sampler2D texture0;
        uniform sampler2D texture1;
        uniform sampler2D texture2;
        uniform vec3 textureLimits;
        varying highp vec3 v_position;
        varying highp vec3 v_normal;
      `,
      saveColor : `
        vec4 diffuseColor_s = vec4(diffuseColor);
      `,
      setTexture: `
      	highp vec3 blending = abs( v_normal );
        blending = normalize(max(blending, 0.0)); // Force weights to sum to 1.0
        float b = (blending.x + blending.y + blending.z);
        blending /= vec3(b, b, b);
        
        float biasFreq = 0.06;
        float biasIntensity = 15.0;
        
        float bias = snoise(vec4(v_position.x, v_position.y, 1.0, 1.0) * biasFreq);
        
        float height = v_position.z + (bias * biasIntensity);

        float blendLength0 = 10.0;
        float blendLength1 = 10.0;
        float blendLength2 = 10.0;
        
        float limitPos0 = textureLimits.x;
        float limitPos1 = textureLimits.y;
        
        float zoom0 = 0.005;
        float zoom1 = 0.005;
        float zoom2 = 0.005;
        
        float intensity1 = smoothstep((limitPos0 - blendLength0), limitPos0, height);
        float intensity2 = smoothstep((limitPos1 - blendLength1), limitPos1, height);
        
        vec4 diffuse0 = vec4(0.0,0.0,0.0,1.0);
        vec4 diffuse1 = vec4(0.0,0.0,0.0,1.0);
        vec4 diffuse2 = vec4(0.0,0.0,0.0,1.0);
        
        diffuse0 = samplePlanar(texture0, v_position, blending, zoom0);
        
        if(intensity1 > 0.0) {
          diffuse1 = samplePlanar(texture1, v_position, blending, zoom1);
          diffuse2 = diffuse1;
        }
        if(intensity2 > 0.0) {
          diffuse2 = samplePlanar(texture2, v_position, blending, zoom2);
        }
        
        vec4 diffuse = mix(diffuse0, diffuse1, intensity1);
        diffuse = mix(diffuse, diffuse2, intensity2);
        
        texelColor = mapTexelToLinear( diffuse );
        
        diffuseColor = diffuseColor_s * texelColor;        
      `
    }

  })

  material = new CustomLambertMaterial(
    {
      //color: 0x607D8B,
      wireframe : false,
      map: texture1
    })

  wireframeMaterial = new THREE.MeshLambertMaterial(
    {
      color: 0x607D8B,
      wireframe : false
    })

  console.log("material defines", material)

  chunk1 = new Chunk(wireframeMaterial, new THREE.Vector3(0, 0, 0), 256);
  scene.add( chunk1.mesh );

  scene.add(new THREE.AmbientLight(0x666666, 0.5));

  let light;

  light = new THREE.DirectionalLight(0xdfebff, 0.8);
  light.position.set(50, -50, 80);
  light.position.multiplyScalar(1.3);

  light.castShadow = true;

  light.shadow.mapSize.width = 1024;
  light.shadow.mapSize.height = 1024;

  let d = 400;

  light.shadow.camera.left = -d;
  light.shadow.camera.right = d;
  light.shadow.camera.top = d;
  light.shadow.camera.bottom = -d;

  light.shadow.camera.near = 0.5;
  light.shadow.camera.far = 200;

  light.shadow.bias = -0.001;

  //let helper = new THREE.CameraHelper( light.shadow.camera );
  //scene.add( helper )

  scene.add(light);

}

const render = function() {

  requestAnimationFrame( render );

  controls.update();

  //mesh.rotation.z += 0.01;

  renderer.render( scene, camera );

}

const noiseCombined = function(x, y, freq, weight, points, index) {
  let noise = evaluateSimplex(x, y, freq, index, globalNoiseZOffset)
  return evaluateRd(noise, points) * weight
}

const evaluateCombinedLayers = function(layers, x, y) {

  let layerValuesById = layers.reduce( (arr, layer, index) => {
    let noise = layer.noise;
    const raw = noiseCombined(x, y, noise.freq, 1, noise.points, layer.id); //weight one
    arr[layer.id] = {
      raw,
      weight : noise.weight
    }
    return arr;
  }, []);

  let total = layers.reduce( (sum, layer) => {
    let layerId = layer.id;
    let maskingLayerId = layer.noise.mask.maskingLayerId;

    let val = layerValuesById[layerId].raw;
    let weight = layerValuesById[layerId].weight;
    if(maskingLayerId) {
      let maskLayerVal = layerValuesById[maskingLayerId].raw;
      let maskRange = layer.noise.mask.range;

      let maskFade = 0.05;
      let maskAmount = smoothStep2(maskLayerVal, maskRange[0] - maskFade, maskRange[0], maskRange[1], maskRange[1] + maskFade);
      val = mix(0.5, val, maskAmount);
    }

    return sum + val * weight

  }, 0)

  return total;
}

const updateObjects = function(state) {
  if(chunk1) {
    updateChunk(chunk1, state);
  }

}

const updateChunk = function({geometry, position}, state) {
  const {noise : {freq, weight, points}, id} = activeLayer(state);
  const {layers} = state;

  let zoom = freq;
  let height = weight;

  if(state.combinedMode) {
    geometry.vertices.forEach(vertex => vertex.z = evaluateCombinedLayers(layers, vertex.x + position.x, vertex.y + position.y))
  } else {
    geometry.vertices.forEach(vertex => vertex.z = noiseCombined(vertex.x + position.x, vertex.y + position.y, zoom, height, points, id))
  }
  //geometry.computeFaceNormals();
  geometry.computeVertexNormals(); //todo normals have discontinuations between chunks

  geometry.verticesNeedUpdate = true;
  geometry.normalsNeedUpdate = true;
}

const updateUniforms = function(state) {
  let totalWeight = state.layers.reduce((acc, l) => acc + l.noise.weight, 0);
  material.uniforms.textureLimits.value.setX(state.renderingSettings.textureLimits[1] * totalWeight);
  material.uniforms.textureLimits.value.setY(state.renderingSettings.textureLimits[2] * totalWeight);
  material.uniforms.textureLimits.needsUpdate = true;
}

const setUpStore = function (store) {
  store.subscribe(() => {
    let newRenderSize = store.getState().renderingSettings.renderSize;
    if(newRenderSize !== previousSize) {
      previousSize = newRenderSize;
      scene.remove(chunk1.mesh);
      chunk1 = new Chunk(material, new THREE.Vector3(0, 0, 0), newRenderSize);
      scene.add( chunk1.mesh );
    }
    let texturesEnabled = store.getState().renderingSettings.texturesEnabled;
    if(texturesEnabled) {
      chunk1.mesh.material = material;
    } else {
      chunk1.mesh.material = wireframeMaterial;
    }
    globalNoiseZOffset = store.getState().renderingSettings.noiseZ;
    updateObjects(store.getState());
    updateUniforms(store.getState());
  });
}


const NoiseGL = {init, updateObjects, render, setUpStore}

export default NoiseGL;