import { evaluateSimplex } from '../logic/noise'
import { getEqualizedRedistributionPoints } from '../logic/redistribution'



const getSamplePoints = function(numPoints, freq) {
  return new Array(numPoints).fill(0).map( (val, i) => evaluateSimplex(i, 1, freq, 0))
}

const getProcessedNoise = function() {

  return {
    freq : 0.015,
    weight : 20,
    points : getEqualizedRedistributionPoints(getSamplePoints(10240, 0.015), 16),
    mask : { range : [0,1], maskingLayerId : null}
  }

}

export default getProcessedNoise