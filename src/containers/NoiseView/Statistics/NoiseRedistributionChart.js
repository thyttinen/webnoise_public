import {evaluateSimplex} from '../../../logic/noise'
import * as React from 'react'
import RedistributionChart from "./RedistributionChart";
import {evaluateRd} from "../../../logic/redistribution";

const getChartData = ({noise, id}) => {
  const points = noise.points;
  const chartData = [];
  for(let i = 0; i < 256; i++) {
    let noiseVal = evaluateSimplex(i, 1, noise.freq, id)
    let rdValue = evaluateRd(noiseVal, points)
    chartData.push({x : i, y1 : noiseVal, y2 : rdValue})
  }
  return chartData
}

const NoiseRedistributionChart = (layer) => (

  RedistributionChart(getChartData(layer))

)

export default NoiseRedistributionChart