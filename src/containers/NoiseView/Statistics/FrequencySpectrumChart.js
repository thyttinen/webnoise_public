import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis } from 'recharts'
import * as React from 'react'
import { Paper, Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui'

const FrequencySpectrumChart = ({chartData, stats}) => (

  <div>

    <Paper zDepth={2} style={{paddingBottom : "10px", marginBottom : "20px", paddingTop : "20px"}}>
      <LineChart width={900} height={200} data={chartData}>
        <XAxis dataKey="freq" tickFormatter={val => val.toFixed(2)}/>
        <YAxis/>
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip label={"label"}/>
        <Line type="monotone" dataKey="amp" stroke="#8884d8" activeDot={{r: 2}} dot={false}/>
      </LineChart>
    </Paper>

    <Table selectable={false}>
      <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
        <TableRow>
          <TableHeaderColumn>Property</TableHeaderColumn>
          <TableHeaderColumn>Value</TableHeaderColumn>
          <TableHeaderColumn>Desc</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        <TableRow>
          <TableRowColumn>Ft</TableRowColumn>
          <TableRowColumn>{stats.highestNonZeroFreq}</TableRowColumn>
          <TableRowColumn>Highest contributing frequency</TableRowColumn>
        </TableRow>
        <TableRow>
          <TableRowColumn>Fh</TableRowColumn>
          <TableRowColumn>{stats.topAmplitudeFreq}</TableRowColumn>
          <TableRowColumn>Frequency with the highest amplitude</TableRowColumn>
        </TableRow>
        <TableRow>
          <TableRowColumn>Fm</TableRowColumn>
          <TableRowColumn>{stats.highestNonZeroFreq / 2}</TableRowColumn>
          <TableRowColumn>Middle frequency</TableRowColumn>
        </TableRow>
      </TableBody>
    </Table>
  </div>


)

export default FrequencySpectrumChart