import {CartesianGrid, Legend, Line, LineChart, Tooltip, XAxis, YAxis} from 'recharts'
import * as React from 'react'
import {Paper} from 'material-ui'

const RedistributionChart = (chartData) => (

  <div>

    <Paper zDepth={2} style={{paddingBottom : "10px", marginBottom : "20px", paddingTop : "20px"}}>
      <LineChart width={900} height={200} data={chartData}>
        <XAxis dataKey="x" tickFormatter={val => val.toFixed(2)}/>
        <YAxis/>
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip label={"label"}/>
        <Line type="monotone" name="Original noise" dataKey="y1" stroke="#8884d8" activeDot={{r: 2}} dot={false}/>
        <Line type="monotone" name="Redistribution" dataKey="y2" stroke="#bf2a5c" activeDot={{r: 2}} dot={false}/>
        <Legend />
      </LineChart>
    </Paper>
  </div>


)

export default RedistributionChart