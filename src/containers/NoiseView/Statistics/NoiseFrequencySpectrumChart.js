import { evaluateSimplex } from '../../../logic/noise'
import * as React from 'react'
import FrequencySpectrumChart from './FrequencySpectrumChart'
import { getFrequencySpectrumInfo, hann, initComplexArray } from '../../../logic/math'

const getChartData = (noise) => {

  const numPoints = 256;

  const data = initComplexArray(numPoints, (i) => {
    return hann(i, numPoints) * (evaluateSimplex(i, 1, noise.freq, 0) - 0.5); //minus noise mean
  })

  return getFrequencySpectrumInfo(data);
}

const NoiseFrequencySpectrumChart = (noise) => (

  FrequencySpectrumChart(getChartData(noise))

)

export default NoiseFrequencySpectrumChart