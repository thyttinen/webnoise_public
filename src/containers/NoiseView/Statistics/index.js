import * as React from 'react'
import { DropDownMenu, MenuItem, TextField } from 'material-ui'
import connect from 'react-redux/es/connect/connect'
import { activeLayer } from '../../../util'
import NoiseFrequencySpectrumChart from './NoiseFrequencySpectrumChart'
import ImageFrequencySpectrumChart from './ImageFrequencySpectrumChart'
import { freqChanged } from '../../../store/actionTypes'
import { store } from '../../../index'
import NoiseRedistributionChart from "./NoiseRedistributionChart";

const FreqInput = connect(
  state =>
    ({
      value: activeLayer(state).noise.freq
    }),
  dispatch =>
    ({
      onChange(event, val) {
        if(val) {
          dispatch(freqChanged(val, store.getState().selectedLayerId))
        }
      }
    })
)(TextField)

class Statistics extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedStatistic: "none"
    };
  }

  handleChange = (event, index, selectedStatistic) => this.setState({selectedStatistic});

  render() {
    return (
      <div className="settings-tab">

        <section>

        <DropDownMenu value={this.state.selectedStatistic} onChange={this.handleChange} style={{width : "100%", marginLeft: "-24px"}} autoWidth={false}>
          <MenuItem value={"none"} primaryText="None"/>
          <MenuItem value={"noise_spectrum"} primaryText="Noise frequency spectrum"/>
          <MenuItem value={"image_spectrum"} primaryText="Reference frequency spectrum"/>
          <MenuItem value={"1d_redistribution"} primaryText="1D noise with redistribution"/>
        </DropDownMenu>

        {this.state.selectedStatistic === "noise_spectrum" && NoiseFrequencySpectrumChart(this.props.activeLayer.noise)}
        {this.state.selectedStatistic === "image_spectrum" && ImageFrequencySpectrumChart(this.props.activeLayer.image)}
        {this.state.selectedStatistic === "1d_redistribution" && NoiseRedistributionChart(this.props.activeLayer)}

        </section>


      </div>
    )}

}

export default connect(
  state =>
    ({
      activeLayer: activeLayer(state),
      layers : state.layers
    }),
  dispatch =>
    ({
      onChange(event, val) {
        dispatch()
      }
    })
)(Statistics);