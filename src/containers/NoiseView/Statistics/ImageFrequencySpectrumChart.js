import * as React from 'react'
import FrequencySpectrumChart from './FrequencySpectrumChart'
import { solveStats } from '../../../logic/solver'

const getChartData = ({heightMap, windowFn}) => {
  return solveStats(heightMap, windowFn);
}

const ImageFrequencySpectrumChart = (image) => (

  (image.heightMap ? FrequencySpectrumChart(getChartData(image)) : "Reference height map not loaded")

)

export default ImageFrequencySpectrumChart