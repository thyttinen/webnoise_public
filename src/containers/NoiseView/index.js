import React from 'react'
import {Tab, Tabs} from 'material-ui/Tabs'
import FontIcon from 'material-ui/FontIcon'
import {ResizableBox} from 'react-resizable'
import muiThemeable from 'material-ui/styles/muiThemeable'
import Layer from './Layer/index'
import Statistics from './Statistics/index'

let draggableOpts = {
  onResize: () => {
    document.dispatchEvent(new Event('resize', {
        'view': window,
        'bubbles': true,
      }
    ))
  }
}

const NoiseViewMenuTabs = ({muiTheme : {baseTheme : {palette}}}) => (
  <Tabs>
    <Tab
      icon={<FontIcon className="material-icons">photo</FontIcon>}
      label="Layer">
      <Layer />
    </Tab>
    <Tab
      icon={<FontIcon className="material-icons">show_chart</FontIcon>}
      label="Statistics">
      <Statistics />
    </Tab>
    <Tab
      icon={<FontIcon className="material-icons">3d_rotation</FontIcon>}
      label="3D Preview">

      <ResizableBox width={Infinity} height={400}
                    minConstraints={[100, 100]}
                    maxConstraints={[Infinity, Infinity]}
                    axis="y" onResize={draggableOpts.onResize}>
        <div id="glCanvas" style={{width : '100%', height : '100%'}}> </div>
      </ResizableBox>
    </Tab>
  </Tabs>
);

export default muiThemeable()(NoiseViewMenuTabs);