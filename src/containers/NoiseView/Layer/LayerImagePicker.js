import { connect } from 'react-redux'
import { setFileHandle } from '../../../store/actionTypes'
import FilePicker from '../../../components/FilePicker/index'

import { store } from '../../../index'

export const LayerImagePicker = connect(
  state => ({
    inputId : "layerImage"
  }),
  dispatch =>
    ({
      onFileLoaded(fileHandle) {
        if(fileHandle) {
          dispatch(setFileHandle(fileHandle, store.getState().selectedLayerId))
        }
      }
    })
)(FilePicker)