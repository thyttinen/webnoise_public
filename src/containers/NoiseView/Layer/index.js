import React from 'react'
import FontIcon from 'material-ui/FontIcon'
import NoiseCanvas from '../../../components/NoiseCanvas/index'
import ImageCanvas from '../../../components/ImageCanvas/index'
import { connect } from 'react-redux'
import {activeLayer, activeLayerIndex, layerById} from '../../../util'
import { LayerImagePicker } from './LayerImagePicker'
import { Card, CardActions, CardHeader, Divider, DropDownMenu, MenuItem, Paper, RaisedButton } from 'material-ui'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { store } from '../../../index'
import {freqChanged, pointsChanged, setHeightMap, setMaskRange, setWindowFn} from '../../../store/actionTypes'
import { getEqualizedRedistributionPoints } from '../../../logic/redistribution'
import constants from '../../../constants'
import {findMaskRange, solveNoise, solveStats} from '../../../logic/solver'

const LayerNoiseCanvas = connect(
  state =>
    ({
      noiseLayer: activeLayer(state).noise,
      layerIndex : activeLayerIndex(state)
    }))(NoiseCanvas)

const LayerImageCanvas = connect(
  state =>
    ({
      imageLayer: activeLayer(state).image
    }),
  dispatch =>
    ({
      onHeightMapLoad(heightMap) {
        dispatch(setHeightMap(heightMap, store.getState().selectedLayerId));
      }
    }))(ImageCanvas)

const computeNoise = () => {
  const {noise : {points, mask}, image : {heightMap, windowFn}, id} = activeLayer(store.getState());
  if(heightMap) {
    const imagePoints = getEqualizedRedistributionPoints(heightMap, points.length);
    const newPoints = points.map(({x,y}, i) => ({x, y : imagePoints[i].y}));

    const info = solveStats(heightMap, windowFn);

    console.log("masking layer id : ", mask.maskingLayerId);
    const maskingLayer = layerById(store.getState(), mask.maskingLayerId);
    console.log("masking layer : ", maskingLayer)
    const maskRange = findMaskRange(heightMap, maskingLayer ? maskingLayer.image.heightMap : null)
    console.log("maskrange : ", maskRange)
    store.dispatch(setMaskRange(maskRange, id))

    // factor for converting dominant freq found in reference to noise frequency
    // e.g 0.03x = 0.015

    //for dominant frequency
    let middleFreq = 0.03;
    //noise frequency is
    let noiseFreq = 0.020

    let factor = noiseFreq / middleFreq;

    const newFreq = info.stats.topAmplitudeFreq * factor;

    store.dispatch(freqChanged(newFreq, id))
    store.dispatch(pointsChanged(newPoints, id));
  }

}


const Layer = ({muiTheme : {baseTheme : {palette}}, activeLayer, onWindowFnChange}) => (
  <div className="noise-tab">
    <div>
      <Paper zDepth={3} style={{marginBottom : "5px", marginRight: "20px", backgroundColor : palette.accent1Color}}>
        <LayerImageCanvas />
      </Paper>
      <LayerImagePicker />
    </div>
    <Paper zDepth={3} style={{backgroundColor : palette.accent1Color}}>
      <LayerNoiseCanvas/>
    </Paper>
    <Card style={{width: "100%", marginLeft: "20px"}}>
      <CardHeader
        title="Noise solver"
        subtitle="Settings"
        actAsExpander={false}
        showExpandableButton={false}
      />
      <CardActions>
        <label>
          <span>Window function</span>
          <DropDownMenu
            className="labeled-input"
            value={activeLayer.image.windowFn}
            onChange={onWindowFnChange}
            style={{width : "120px"}}
            autoWidth={false}>
            <MenuItem value={constants.WINDOW_NONE} primaryText="None" />
            <MenuItem value={constants.WINDOW_HANN} primaryText="Hann" />
            <MenuItem value={constants.WINDOW_HAMMING} primaryText="Hamming" />
            <MenuItem value={constants.WINDOW_FLAT_TOP} primaryText="FlatTop" />
          </DropDownMenu>
        </label>
        <Divider/>
        <RaisedButton label="Compute" primary={true}
                      onClick={computeNoise}
                      style={{marginTop : "20px"}}
                      disabled={!activeLayer.image.heightMap}
                      icon={<FontIcon className="material-icons">arrow_forward</FontIcon>} />
      </CardActions>
    </Card>
  </div>
);

export default connect(
  state =>
    ({
      activeLayer: activeLayer(state),
    }),
  dispatch =>
    ({
      onWindowFnChange(event, index, windowFn) {
        dispatch(setWindowFn(windowFn, store.getState().selectedLayerId));
      }
    }))(muiThemeable()(Layer));