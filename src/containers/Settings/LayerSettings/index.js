import React from 'react'
import { Divider, Paper, Slider, TextField } from 'material-ui'
import { freqChanged, weightChanged } from '../../../store/actionTypes'
import { connect } from 'react-redux'
import { clamp, linearToLog, logToLinear } from '../../../logic/math'
import { PointsDragBar } from './PointsDragBar'
import { activeLayer } from '../../../util'
import { store } from '../../../index'

const FREQ_RANGE = {min : 0.005, max : 0.2}

const FreqSlider = connect(
  state =>
    ({
      value: clamp(logToLinear(activeLayer(state).noise.freq, 0, 100, FREQ_RANGE.min, FREQ_RANGE.max), 0, 100)
    }),
  dispatch =>
    ({
      onChange(event, val) {
        dispatch(freqChanged(linearToLog(val, 0, 100, FREQ_RANGE.min, FREQ_RANGE.max), store.getState().selectedLayerId))
      }
    })
)(Slider)

const FreqInput = connect(
  state =>
    ({
      value: activeLayer(state).noise.freq
    }),
  dispatch =>
    ({
      onChange(event, val) {
        if(val) {
          dispatch(freqChanged(val, store.getState().selectedLayerId))
        }
      }
    })
)(TextField)

const WeightSlider = connect(
  state =>
    ({
      value: activeLayer(state).noise.weight
    }),
  dispatch =>
    ({
      onChange(event, val) {
        dispatch(weightChanged(val, store.getState().selectedLayerId))
      }
    })
)(Slider)

const LayerSettings = () => (
<div>
  <div className="row">
    <div className="col-xs-4">
      <section style={{paddingRight : 0}}>
        <label>
          <span>f<sub>noise</sub></span>
          <FreqInput id="fn" className="labeled-input" style={{width : "140px"}} />
        </label>
        <FreqSlider className="compact-slider" axis="x" min={0} max={100} />
        <Divider />
        <label>w<sub>noise</sub></label>
        <WeightSlider className="compact-slider" axis="x" min={0} max={100} />
        <Divider />
      </section>
    </div>
    <div className="col-xs-8">
      <Paper zDepth={1} style={{margin : '20px 20px 0px 0px'}}>
        <PointsDragBar />
      </Paper>
      <label>redistribution</label>
    </div>
  </div>
</div>
);

export default LayerSettings;