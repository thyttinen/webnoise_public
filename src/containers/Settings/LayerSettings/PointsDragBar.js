import { connect } from 'react-redux'
import {pointsChanged} from '../../../store/actionTypes'
import DragBarControl from '../../../components/DragBar/index'
import { activeLayer } from '../../../util'

import {store} from '../../../index'

function getPoints (state) {
  return [...activeLayer(state).noise.points]
}
export const PointsDragBar = connect(
  state =>
    ({
      points: getPoints(state)
    }),
  dispatch =>
    ({
      onPointsChange(points) {
        dispatch(pointsChanged(points, store.getState().selectedLayerId))
      }
    })
)(DragBarControl)