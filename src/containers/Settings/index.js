import React from 'react'
import {Tab, Tabs} from 'material-ui/Tabs'
import FontIcon from 'material-ui/FontIcon'
import Masking from './Masking/index'
import Rendering from './Rendering/index'
import LayerSettings from './LayerSettings/index'

const SettingsMenuTabs = () => (
  <Tabs>
    <Tab
      icon={<FontIcon className="material-icons">settings</FontIcon>}
      label="Settings">
        <LayerSettings/>
    </Tab>
    <Tab
      icon={<FontIcon className="material-icons">settings</FontIcon>}
      label="Masking">
        <Masking />
    </Tab>
    <Tab
      icon={<FontIcon className="material-icons">3d_rotation</FontIcon>}
      label="Rendering">
        <Rendering />
    </Tab>
  </Tabs>
);

export default SettingsMenuTabs;