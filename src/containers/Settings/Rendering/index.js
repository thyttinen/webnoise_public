import React from 'react'
import {connect} from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import 'rc-slider/assets/index.css';
import {Range} from "rc-slider";
import {setNoiseZ, setRenderSize, setTextureLimits, setTexturesEnabled} from "../../../store/actionTypes";
import {Divider, Slider, Toggle} from "material-ui";

const RenderDistanceSlider = connect(
  state =>
    ({
      value: state.renderingSettings.renderSize
    }),
  dispatch =>
    ({
      onChange(event, value) {
        dispatch(setRenderSize(value))
      }
    })
)(Slider)

const NoiseZSlider = connect(
  state =>
    ({
      value: state.renderingSettings.noiseZ
    }),
  dispatch =>
    ({
      onChange(event, value) {
        dispatch(setNoiseZ(value))
      }
    })
)(Slider)

const TexturingToggle = connect(
  state =>
    ({
      value: state.renderingSettings.texturesEnabled
    }),
  dispatch =>
    ({
      onToggle(event, value) {
        dispatch(setTexturesEnabled(value))
      }
    })
)(Toggle)


const Rendering = ({muiTheme : {baseTheme : {palette}}, renderingSettings, onTextureLimitsChanged}) => (
  <div>

    <div className="row">
      <div className="col-xs-6">

        <section>

          <label>
            <span>Texture limits</span>
          </label>

          <Range min={0} max={1} step={0.01}
                 style={{marginBottom : "15px"}}
                 value={renderingSettings.textureLimits}
                 onChange={val => onTextureLimitsChanged(val)}
                 railStyle={{backgroundColor : "rgb(207, 216, 220)"}}
                 trackStyle={[{backgroundColor : "#607D8B"}, {backgroundColor : "#607D8B"}, {backgroundColor : "#607D8B"}]}
                 handleStyle={[{backgroundColor : "#607D8B", borderColor : "#607D8B"}, {backgroundColor : "#607D8B", borderColor : "#607D8B"}
                   , {backgroundColor : "#607D8B", borderColor : "#607D8B"}, {backgroundColor : "#607D8B", borderColor : "#607D8B"}]}/>

          <Divider />

          <label>
            <TexturingToggle
              label="Textures enabled"
              className="labeled-input"
              style={{width : "180px"}}/>
          </label>

        </section>


      </div>
      <div className="col-xs-6">

        <section>

          <label>
            <span>Render distance</span>
          </label>

          <RenderDistanceSlider className="compact-slider" axis="x" min={256} max={1024} />

          <Divider />

          <label>
            <span>Noise z-offset</span>
          </label>

          <NoiseZSlider className="compact-slider" axis="x" min={0} max={10} />

        </section>

      </div>
    </div>

  </div>
);

export default connect(
  state =>
    ({
      renderingSettings: state.renderingSettings
    }),
  dispatch =>
    ({
      onTextureLimitsChanged(textureLimits) {
        dispatch(setTextureLimits(textureLimits));
      }
    }))(muiThemeable()(Rendering));