/* eslint-disable no-undef */
import React from 'react'
import {connect} from 'react-redux'
import {activeLayer} from '../../../util'
import muiThemeable from 'material-ui/styles/muiThemeable'
import 'rc-slider/assets/index.css';
import {Range} from "rc-slider";
import {setMaskingLayerId, setMaskRange} from "../../../store/actionTypes";
import {Divider, DropDownMenu, MenuItem} from "material-ui";

const _createMenuItems = (layers) => {
  let menuItems = [(<MenuItem
    value={null}
    key={-1}
    primaryText={"None"} />)];
  for (let layer of layers) {
    let itemIndex = layers.indexOf(layer);
    let item = (
      <MenuItem
        value={layer.id}
        key={layer.id}
        primaryText={"Layer: " + (itemIndex+1)} />
    );
    menuItems.push(item);
  }
  return menuItems;
}


const Masking = ({muiTheme : {baseTheme : {palette}}, activeLayer, layers, onMaskRangeChanged, onMaskingLayerChanged}) => (
    <section style={{maxWidth : "400px"}}>
      
      <label>
        <span>Mask range</span>
      </label>
      <Range min={0} max={1} step={0.01}
             value={activeLayer.noise.mask.range}
             onChange={val => onMaskRangeChanged(val, activeLayer.id)}
             railStyle={{backgroundColor : "rgb(207, 216, 220)"}}
             trackStyle={[{backgroundColor : "#607D8B"}]}
             handleStyle={[{backgroundColor : "#607D8B", borderColor : "#607D8B"}, {backgroundColor : "#607D8B", borderColor : "#607D8B"}]}
      />

      <label>
        <span>Masked by layer</span>
        <DropDownMenu
          className="labeled-input"
          value={activeLayer.noise.mask.maskingLayerId}
          onChange={onMaskingLayerChanged(activeLayer.id)}>
          { _createMenuItems(layers) }
        </DropDownMenu>
      </label>

      <Divider />
    </section>
);

export default connect(
  state =>
    ({
      activeLayer: activeLayer(state),
      layers: state.layers
    }),
  dispatch =>
    ({
      onMaskRangeChanged(range, id) {
        console.log("changed", range)
        dispatch(setMaskRange(range, id))
      },
      onMaskingLayerChanged : (activeLayerId) => (e, index, value) => {
        console.log("changed", value)
        dispatch(setMaskingLayerId(value, activeLayerId))
      }
    }))(muiThemeable()(Masking));