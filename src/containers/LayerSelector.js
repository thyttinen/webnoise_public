import React from 'react'
import {Badge, Dialog, Divider, FlatButton, FloatingActionButton, FontIcon, RaisedButton, Toggle} from 'material-ui'
import {List, ListItem} from 'material-ui/List';
import {connect} from 'react-redux'
import {addLayer, deleteLayer, loadState, saveState, selectLayer, setCombinedMode} from '../store/actionTypes'
import {store} from '../index'
import SaveDialog from "./SaveDialog"
import FilePicker from "../components/FilePicker/index"

const activeLayerStyle = {backgroundColor : "rgb(216, 216, 216)"}

const stopPropagationAnd = (fn) => event => {event.stopPropagation(); return fn()}

const StateFilePicker = connect(
  state => ({}),
  dispatch =>
    ({
      onFileLoaded(fileHandle) {
        if(fileHandle) {
          let reader = new FileReader();
          reader.readAsText(fileHandle);
          reader.onload = function(e) {
            let state = JSON.parse(reader.result)
            store.dispatch(loadState(state))
          }
        }
      }
    })
)(FilePicker)

const LayerSelector = ({layers, selectedLayerId, combinedMode, onLayerSelection, onDeleteLayer, onAddLayer, onCombinedModeToggled}) => (
  <div style={{textAlign : 'center', position : 'relative', height : "100%"}}>

    <List>
      {layers.map(function(layer, i){
        return <ListItem className="layer-selector"
                         primaryText={"Layer " + (i+1)} leftIcon={<FontIcon className="material-icons">{"filter_" + (i+1)}</FontIcon>}
                         style={layer.id === selectedLayerId ? activeLayerStyle : null}
                         onClick={onLayerSelection(layer)}
                         key={i}>
                  {layer.id !== selectedLayerId &&
                  <div onClick={stopPropagationAnd(onDeleteLayer(layer))} style={{position : 'absolute', top : '16px', right : '4px'}}>
                    <FontIcon style={{fontSize : "15px"}} className="material-icons">remove_circle_outline</FontIcon>
                  </div>}
                </ListItem>
      })}
    </List>
    <Divider style={{marginBottom : "20px"}} />
    <RaisedButton label="Add layer" primary={true}
                  onClick={onAddLayer()}
                  icon={<FontIcon className="material-icons">add</FontIcon>} />
    <Divider style={{marginTop : "20px"}} />

    <section>
      <label>
        <Toggle
          label="Combined"
          className="labeled-input"
          onToggle={onCombinedModeToggled}
        />
      </label>
    </section>

    <div style={{position : "absolute", bottom : "0px"}}>
      <StateFilePicker />
      <SaveDialog  />
    </div>

  </div>
);

export default connect(
  state =>
    ({
      selectedLayerId : state.selectedLayerId,
      layers: [...state.layers],
      combinedMode : state.combinedMode
    }),
  dispatch =>
    ({
      onLayerSelection(layer) {
        return () => dispatch(selectLayer(layer.id))
      },
      onDeleteLayer(layer) {
        return () => dispatch(deleteLayer(layer.id))
      },
      onAddLayer() {
        return () => dispatch(addLayer())
      },
      onCombinedModeToggled(event, active) {
        return dispatch(setCombinedMode(active))
      }
    })
)(LayerSelector);