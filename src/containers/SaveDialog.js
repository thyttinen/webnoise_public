import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {store} from '../index'

export default class SaveDialog extends React.Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onClick={this.handleClose}
      />
    ];

    return (
      <div className="bottom-buttons">
        <RaisedButton label="Save" onClick={this.handleOpen} />
        <Dialog
          title="Save state"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <textarea style={{width : "100%", height : "200px"}}>{JSON.stringify(store.getState())}</textarea>
        </Dialog>
      </div>
    );
  }
}